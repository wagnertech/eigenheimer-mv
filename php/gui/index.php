<?php
//phpinfo();

// define user
$user = getenv("USER");
if ($user == "") putenv("USER=gui");

// includes
$path = preg_replace("/mVersicherung.*/", "mVersicherung", __FILE__);
set_include_path($path . PATH_SEPARATOR . get_include_path());
require_once 'util/Logger.php';
require_once 'util/Config.php';
require_once 'GenericAdmin/gui/control/GenadControl.php';

// load configuration + class loader
include 'gui/ConfigurationData.php';
Config::setConfiguration($configurationData);

// Logging
$logger = Logger::getInstance();
$requestMethod = $_SERVER["REQUEST_METHOD"];
$logger->log(__FILE__, $requestMethod);
$logger->log(__FILE__, "GET-Variabeles:");
while ( list($key, $value) = each($_GET) ) {
	$logger->log(__FILE__, $key . " : " . $value);;
}
$logger->log(__FILE__, "POST-Variabeles:");
while ( list($key, $value) = each($_POST) ) {
	$logger->log(__FILE__, $key . " : " . $value);;
}

// call implementation
try {
	$control = new GenadControl();
	$control->doIt();
}
catch (Exception $e) {
	$logger->logException(__FILE__, $e);	
	echo "<html><body>Error occurred!</body></html>";
}

