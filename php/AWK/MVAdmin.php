<?php

// imports
require_once 'GenericAdmin/GenericAdmin.php';
require_once 'util/Config.php';

class MVAdmin extends GenericAdmin {

	function __construct() {
		// Generic Administration configuration data
		require 'AWK/impl/GenadData.php';
		parent::setDscrData($mocd_tab, $atdc_tab);
	}
}
