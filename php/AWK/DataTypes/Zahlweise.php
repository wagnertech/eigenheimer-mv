<?php

// imports
require_once 'GenericAdmin/DataTypes/EnumType.php';

class Zahlweise extends EnumType {

	/** Zahlungsmethoden */
	const ZW_BAR = 0;
	const ZW_SELBST  = 1;
	const ZW_EINZUG   = 2;
	
	// (string representations of) defined values
	public static $defd_vals = array (
	    self::ZW_BAR => 'Barzahler',
	    self::ZW_SELBST  => 'Selbstüberweise',
	    self::ZW_EINZUG   => 'Bankeinzug',
	);

	public function __construct($value) {
		$this->initInstance('Zahlweise', self::$defd_vals, $value);
	}
}
