<?php

// imports
require_once 'GenericAdmin/DataTypes/AttrProperty.php';

	// Generic Administration configuration data
	$mocd_tab = array (
		1 => array (
			'intern' => 'Stamm',
			'extern' => 'Personenstamm'),
		2 => array (                          // das sind keine Entitäten
			'intern' => 'VersichertesObjekt',
			'extern' => 'VersichertesObjekt',
		    'props' => array(ClassProperty::CP_CREATE, ClassProperty::CP_SET, ClassProperty::CP_DELETE),
		),
		3 => array (
			'intern' => 'Beitrage',
			'extern' => 'Beiträge',
	        'props' => array(ClassProperty::CP_CREATE, ClassProperty::CP_SET, ClassProperty::CP_DELETE),
		),
	);

	$atdc_tab = array (

	    // Personenstamm
		array (
			'intern'  => 'nachname',
			'extern'  => 'Nachname',
			'moc'     => 'Stamm',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		array (
			'intern'  => 'vorname',
			'extern'  => 'Vorname',
			'moc'     => 'Stamm',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		array (
			'intern'  => 'stasse',
			'extern'  => 'Straße',
			'moc'     => 'Stamm',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		array (
			'intern'  => 'plz',
			'extern'  => 'PLZ',
			'moc'     => 'Stamm',
			'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		array (
			'intern'  => 'ort',
			'extern'  => 'Ort',
			'moc'     => 'Stamm',
			'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'zahlweise',
	        'extern'  => 'Zahlweise',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET),
	        'typ_cl'  => 'Zahlweise'),
	    array (
	        'intern'  => 'iban',
	        'extern'  => 'IBAN',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'bic',
	        'extern'  => 'BIC',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'kommentar',
	        'extern'  => 'Kommentar',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'kundigung',
	        'extern'  => 'Kundigung',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET, AttrProperty::AP_INIT)),
	    array (
	        'intern'  => 'vers_objs',
	        'extern'  => 'Versicherte_Objekte',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET, AttrProperty::AP_LIST, AttrProperty::AP_NOMOC),
	        'rel_moc' => 2, // VO
	    ),
	    
	    // VersichertesObjekt
	    array (
            'intern'  => 'stamm_id',
            'extern'  => 'stamm_id',
            'moc'     => 'VersichertesObjekt',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_DEF_FK),
            'rel_moc' => 1), //Stamm
	    array (
	        'intern'  => 'versicherungsnummer',
	        'extern'  => 'Versicherungsnummer',
	        'moc'     => 'VersichertesObjekt',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'beschreibung',
	        'extern'  => 'Beschreibung',
	        'moc'     => 'VersichertesObjekt',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET),
	    ),
	    array (
	        'intern'  => 'kommentar',
	        'extern'  => 'Kommentar',
	        'moc'     => 'VersichertesObjekt',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'basispramie',
	        'extern'  => 'Basisprämie',
	        'moc'     => 'VersichertesObjekt',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET, AttrProperty::AP_FLOAT)),
	    array (
	        'intern'  => 'zahlweise',
	        'extern'  => 'Zahlweise',
	        'moc'     => 'VersichertesObjekt',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET),
	        'typ_cl'  => 'Zahlweise'),
	    array (
	        'intern'  => 'iban',
	        'extern'  => 'IBAN',
	        'moc'     => 'VersichertesObjekt',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'bic',
	        'extern'  => 'BIC',
	        'moc'     => 'VersichertesObjekt',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'beitrag',
	        'extern'  => 'Zahlungen',
	        'moc'     => 'VersichertesObjekt',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET, AttrProperty::AP_LIST, AttrProperty::AP_NOMOC),
	        'rel_moc' => 3), //Zahlungen
	    
	    // Beiträge
	    array (
	        'intern'  => 'vo_id',
	        'extern'  => 'VO_id',
	        'moc'     => 'Beitrage',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_DEF_FK),
	        'rel_moc' => 2), //VersichertesObjekt
	    array (
			'intern'  => 'jahr',
			'extern'  => 'Jahr',
			'moc'     => 'Beitrage',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_INT)),
	    array (
	        'intern'  => 'zahlweise',
	        'extern'  => 'Zahlweise',
	        'moc'     => 'Beitrage',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET),
	        'typ_cl'  => 'Zahlweise'),
	    array (
	        'intern'  => 'betrag',
	        'extern'  => 'Betrag',
	        'moc'     => 'Beitrage',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET, AttrProperty::AP_FLOAT),
	    ),
	    array (
	        'intern'  => 'kommentar',
	        'extern'  => 'Kommentar',
	        'moc'     => 'Beitrage',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		);

?>
