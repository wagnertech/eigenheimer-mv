<?php



/**
 * This class defines the structure of the 'beitrage' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.propel.map
 */
class BeitrageTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'propel.map.BeitrageTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('beitrage');
        $this->setPhpName('Beitrage');
        $this->setClassname('Beitrage');
        $this->setPackage('propel');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('mod', 'Mod', 'INTEGER', true, null, null);
        $this->addColumn('jahr', 'Jahr', 'INTEGER', true, null, null);
        $this->addColumn('zahlweise', 'Zahlweise', 'INTEGER', true, null, null);
        $this->addColumn('betrag', 'Betrag', 'FLOAT', true, null, null);
        $this->addColumn('kommentar', 'Kommentar', 'VARCHAR', true, 1000, null);
        $this->addForeignKey('vo_id', 'VoId', 'INTEGER', 'versichertes_objekt', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('VersichertesObjekt', 'VersichertesObjekt', RelationMap::MANY_TO_ONE, array('vo_id' => 'id', ), null, null);
    } // buildRelations()

} // BeitrageTableMap
