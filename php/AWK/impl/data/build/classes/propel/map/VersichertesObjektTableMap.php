<?php



/**
 * This class defines the structure of the 'versichertes_objekt' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.propel.map
 */
class VersichertesObjektTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'propel.map.VersichertesObjektTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('versichertes_objekt');
        $this->setPhpName('VersichertesObjekt');
        $this->setClassname('VersichertesObjekt');
        $this->setPackage('propel');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('mod', 'Mod', 'INTEGER', true, null, null);
        $this->addColumn('versicherungsnummer', 'Versicherungsnummer', 'INTEGER', true, null, null);
        $this->addColumn('beschreibung', 'Beschreibung', 'VARCHAR', true, 200, null);
        $this->addColumn('kommentar', 'Kommentar', 'VARCHAR', true, 1000, null);
        $this->addForeignKey('stamm_id', 'StammId', 'INTEGER', 'stamm', 'id', true, null, null);
        $this->addColumn('basispramie', 'Basispramie', 'FLOAT', true, null, null);
        $this->addColumn('zahlweise', 'Zahlweise', 'INTEGER', true, null, null);
        $this->addColumn('iban', 'Iban', 'CHAR', true, 20, null);
        $this->addColumn('bic', 'Bic', 'CHAR', true, 10, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stamm', 'Stamm', RelationMap::MANY_TO_ONE, array('stamm_id' => 'id', ), 'CASCADE', null);
        $this->addRelation('Beitrag', 'Beitrage', RelationMap::ONE_TO_MANY, array('id' => 'vo_id', ), null, null, 'Beitrags');
    } // buildRelations()

} // VersichertesObjektTableMap
