<?php



/**
 * Skeleton subclass for representing a row from the 'stamm' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.propel
 */
class Stamm extends BaseStamm
{
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME) {
        if ($name == 'vers_objs') ; // do nothing
        else parent::setByName($name, $value, $type);
    }
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME) {
        if ($name == 'vers_objs') {
            $vos = $this->getVersichertesObjekts();
            $voids = array();
            foreach ($vos as $vo) $voids[] = $vo->getId();
            return $voids;
        }
        else return parent::getByName($name, $type);
    }
    public function getZahlweise() {
        return new Zahlweise(parent::getZahlweise());
    }
    public function setZahlweise($v) {
        parent::setZahlweise($v->getValue());
    }
    public function getKundigung() {
        $raw = parent::getKundigung();
        if ($raw == 0) return ""; // keine Kündigung
        return date("Y-m-d", $raw);;
    }
    public function setKundigung($v) {
        if ($v == "") parent::setKundigung(0);
        else parent::setKundigung(strtotime($v));
    }
    public function save(PropelPDO $con = null)
    {
        if ($this->isNew() || $this->mod >= 999999) {
            $this->setMod(0);
        } else {
            $this->setMod($this->mod + 1);
        }
        return parent::save($con);
    }
    
}
