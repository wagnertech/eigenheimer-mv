<?php


/**
 * Base class that represents a query for the 'stamm' table.
 *
 *
 *
 * @method StammQuery orderById($order = Criteria::ASC) Order by the id column
 * @method StammQuery orderByMod($order = Criteria::ASC) Order by the mod column
 * @method StammQuery orderByNachname($order = Criteria::ASC) Order by the nachname column
 * @method StammQuery orderByVorname($order = Criteria::ASC) Order by the vorname column
 * @method StammQuery orderByStasse($order = Criteria::ASC) Order by the stasse column
 * @method StammQuery orderByPlz($order = Criteria::ASC) Order by the plz column
 * @method StammQuery orderByOrt($order = Criteria::ASC) Order by the ort column
 * @method StammQuery orderByZahlweise($order = Criteria::ASC) Order by the zahlweise column
 * @method StammQuery orderByIban($order = Criteria::ASC) Order by the iban column
 * @method StammQuery orderByBic($order = Criteria::ASC) Order by the bic column
 * @method StammQuery orderByKommentar($order = Criteria::ASC) Order by the kommentar column
 * @method StammQuery orderByKundigung($order = Criteria::ASC) Order by the kundigung column
 *
 * @method StammQuery groupById() Group by the id column
 * @method StammQuery groupByMod() Group by the mod column
 * @method StammQuery groupByNachname() Group by the nachname column
 * @method StammQuery groupByVorname() Group by the vorname column
 * @method StammQuery groupByStasse() Group by the stasse column
 * @method StammQuery groupByPlz() Group by the plz column
 * @method StammQuery groupByOrt() Group by the ort column
 * @method StammQuery groupByZahlweise() Group by the zahlweise column
 * @method StammQuery groupByIban() Group by the iban column
 * @method StammQuery groupByBic() Group by the bic column
 * @method StammQuery groupByKommentar() Group by the kommentar column
 * @method StammQuery groupByKundigung() Group by the kundigung column
 *
 * @method StammQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method StammQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method StammQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method StammQuery leftJoinVersichertesObjekt($relationAlias = null) Adds a LEFT JOIN clause to the query using the VersichertesObjekt relation
 * @method StammQuery rightJoinVersichertesObjekt($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VersichertesObjekt relation
 * @method StammQuery innerJoinVersichertesObjekt($relationAlias = null) Adds a INNER JOIN clause to the query using the VersichertesObjekt relation
 *
 * @method Stamm findOne(PropelPDO $con = null) Return the first Stamm matching the query
 * @method Stamm findOneOrCreate(PropelPDO $con = null) Return the first Stamm matching the query, or a new Stamm object populated from the query conditions when no match is found
 *
 * @method Stamm findOneByMod(int $mod) Return the first Stamm filtered by the mod column
 * @method Stamm findOneByNachname(string $nachname) Return the first Stamm filtered by the nachname column
 * @method Stamm findOneByVorname(string $vorname) Return the first Stamm filtered by the vorname column
 * @method Stamm findOneByStasse(string $stasse) Return the first Stamm filtered by the stasse column
 * @method Stamm findOneByPlz(string $plz) Return the first Stamm filtered by the plz column
 * @method Stamm findOneByOrt(string $ort) Return the first Stamm filtered by the ort column
 * @method Stamm findOneByZahlweise(int $zahlweise) Return the first Stamm filtered by the zahlweise column
 * @method Stamm findOneByIban(string $iban) Return the first Stamm filtered by the iban column
 * @method Stamm findOneByBic(string $bic) Return the first Stamm filtered by the bic column
 * @method Stamm findOneByKommentar(string $kommentar) Return the first Stamm filtered by the kommentar column
 * @method Stamm findOneByKundigung(int $kundigung) Return the first Stamm filtered by the kundigung column
 *
 * @method array findById(int $id) Return Stamm objects filtered by the id column
 * @method array findByMod(int $mod) Return Stamm objects filtered by the mod column
 * @method array findByNachname(string $nachname) Return Stamm objects filtered by the nachname column
 * @method array findByVorname(string $vorname) Return Stamm objects filtered by the vorname column
 * @method array findByStasse(string $stasse) Return Stamm objects filtered by the stasse column
 * @method array findByPlz(string $plz) Return Stamm objects filtered by the plz column
 * @method array findByOrt(string $ort) Return Stamm objects filtered by the ort column
 * @method array findByZahlweise(int $zahlweise) Return Stamm objects filtered by the zahlweise column
 * @method array findByIban(string $iban) Return Stamm objects filtered by the iban column
 * @method array findByBic(string $bic) Return Stamm objects filtered by the bic column
 * @method array findByKommentar(string $kommentar) Return Stamm objects filtered by the kommentar column
 * @method array findByKundigung(int $kundigung) Return Stamm objects filtered by the kundigung column
 *
 * @package    propel.generator.propel.om
 */
abstract class BaseStammQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseStammQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'mversicherung';
        }
        if (null === $modelName) {
            $modelName = 'Stamm';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new StammQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   StammQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return StammQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof StammQuery) {
            return $criteria;
        }
        $query = new StammQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Stamm|Stamm[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = StammPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(StammPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Stamm A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Stamm A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `mod`, `nachname`, `vorname`, `stasse`, `plz`, `ort`, `zahlweise`, `iban`, `bic`, `kommentar`, `kundigung` FROM `stamm` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Stamm();
            $obj->hydrate($row);
            StammPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Stamm|Stamm[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Stamm[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StammPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StammPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(StammPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(StammPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StammPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the mod column
     *
     * Example usage:
     * <code>
     * $query->filterByMod(1234); // WHERE mod = 1234
     * $query->filterByMod(array(12, 34)); // WHERE mod IN (12, 34)
     * $query->filterByMod(array('min' => 12)); // WHERE mod >= 12
     * $query->filterByMod(array('max' => 12)); // WHERE mod <= 12
     * </code>
     *
     * @param     mixed $mod The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByMod($mod = null, $comparison = null)
    {
        if (is_array($mod)) {
            $useMinMax = false;
            if (isset($mod['min'])) {
                $this->addUsingAlias(StammPeer::MOD, $mod['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mod['max'])) {
                $this->addUsingAlias(StammPeer::MOD, $mod['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StammPeer::MOD, $mod, $comparison);
    }

    /**
     * Filter the query on the nachname column
     *
     * Example usage:
     * <code>
     * $query->filterByNachname('fooValue');   // WHERE nachname = 'fooValue'
     * $query->filterByNachname('%fooValue%'); // WHERE nachname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nachname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByNachname($nachname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nachname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nachname)) {
                $nachname = str_replace('*', '%', $nachname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StammPeer::NACHNAME, $nachname, $comparison);
    }

    /**
     * Filter the query on the vorname column
     *
     * Example usage:
     * <code>
     * $query->filterByVorname('fooValue');   // WHERE vorname = 'fooValue'
     * $query->filterByVorname('%fooValue%'); // WHERE vorname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $vorname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByVorname($vorname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($vorname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $vorname)) {
                $vorname = str_replace('*', '%', $vorname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StammPeer::VORNAME, $vorname, $comparison);
    }

    /**
     * Filter the query on the stasse column
     *
     * Example usage:
     * <code>
     * $query->filterByStasse('fooValue');   // WHERE stasse = 'fooValue'
     * $query->filterByStasse('%fooValue%'); // WHERE stasse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $stasse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByStasse($stasse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($stasse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $stasse)) {
                $stasse = str_replace('*', '%', $stasse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StammPeer::STASSE, $stasse, $comparison);
    }

    /**
     * Filter the query on the plz column
     *
     * Example usage:
     * <code>
     * $query->filterByPlz('fooValue');   // WHERE plz = 'fooValue'
     * $query->filterByPlz('%fooValue%'); // WHERE plz LIKE '%fooValue%'
     * </code>
     *
     * @param     string $plz The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByPlz($plz = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($plz)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $plz)) {
                $plz = str_replace('*', '%', $plz);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StammPeer::PLZ, $plz, $comparison);
    }

    /**
     * Filter the query on the ort column
     *
     * Example usage:
     * <code>
     * $query->filterByOrt('fooValue');   // WHERE ort = 'fooValue'
     * $query->filterByOrt('%fooValue%'); // WHERE ort LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ort The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByOrt($ort = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ort)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ort)) {
                $ort = str_replace('*', '%', $ort);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StammPeer::ORT, $ort, $comparison);
    }

    /**
     * Filter the query on the zahlweise column
     *
     * Example usage:
     * <code>
     * $query->filterByZahlweise(1234); // WHERE zahlweise = 1234
     * $query->filterByZahlweise(array(12, 34)); // WHERE zahlweise IN (12, 34)
     * $query->filterByZahlweise(array('min' => 12)); // WHERE zahlweise >= 12
     * $query->filterByZahlweise(array('max' => 12)); // WHERE zahlweise <= 12
     * </code>
     *
     * @param     mixed $zahlweise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByZahlweise($zahlweise = null, $comparison = null)
    {
        if (is_array($zahlweise)) {
            $useMinMax = false;
            if (isset($zahlweise['min'])) {
                $this->addUsingAlias(StammPeer::ZAHLWEISE, $zahlweise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($zahlweise['max'])) {
                $this->addUsingAlias(StammPeer::ZAHLWEISE, $zahlweise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StammPeer::ZAHLWEISE, $zahlweise, $comparison);
    }

    /**
     * Filter the query on the iban column
     *
     * Example usage:
     * <code>
     * $query->filterByIban('fooValue');   // WHERE iban = 'fooValue'
     * $query->filterByIban('%fooValue%'); // WHERE iban LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iban The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByIban($iban = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iban)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $iban)) {
                $iban = str_replace('*', '%', $iban);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StammPeer::IBAN, $iban, $comparison);
    }

    /**
     * Filter the query on the bic column
     *
     * Example usage:
     * <code>
     * $query->filterByBic('fooValue');   // WHERE bic = 'fooValue'
     * $query->filterByBic('%fooValue%'); // WHERE bic LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bic The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByBic($bic = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bic)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bic)) {
                $bic = str_replace('*', '%', $bic);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StammPeer::BIC, $bic, $comparison);
    }

    /**
     * Filter the query on the kommentar column
     *
     * Example usage:
     * <code>
     * $query->filterByKommentar('fooValue');   // WHERE kommentar = 'fooValue'
     * $query->filterByKommentar('%fooValue%'); // WHERE kommentar LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kommentar The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByKommentar($kommentar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kommentar)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kommentar)) {
                $kommentar = str_replace('*', '%', $kommentar);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(StammPeer::KOMMENTAR, $kommentar, $comparison);
    }

    /**
     * Filter the query on the kundigung column
     *
     * Example usage:
     * <code>
     * $query->filterByKundigung(1234); // WHERE kundigung = 1234
     * $query->filterByKundigung(array(12, 34)); // WHERE kundigung IN (12, 34)
     * $query->filterByKundigung(array('min' => 12)); // WHERE kundigung >= 12
     * $query->filterByKundigung(array('max' => 12)); // WHERE kundigung <= 12
     * </code>
     *
     * @param     mixed $kundigung The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function filterByKundigung($kundigung = null, $comparison = null)
    {
        if (is_array($kundigung)) {
            $useMinMax = false;
            if (isset($kundigung['min'])) {
                $this->addUsingAlias(StammPeer::KUNDIGUNG, $kundigung['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kundigung['max'])) {
                $this->addUsingAlias(StammPeer::KUNDIGUNG, $kundigung['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StammPeer::KUNDIGUNG, $kundigung, $comparison);
    }

    /**
     * Filter the query by a related VersichertesObjekt object
     *
     * @param   VersichertesObjekt|PropelObjectCollection $versichertesObjekt  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 StammQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVersichertesObjekt($versichertesObjekt, $comparison = null)
    {
        if ($versichertesObjekt instanceof VersichertesObjekt) {
            return $this
                ->addUsingAlias(StammPeer::ID, $versichertesObjekt->getStammId(), $comparison);
        } elseif ($versichertesObjekt instanceof PropelObjectCollection) {
            return $this
                ->useVersichertesObjektQuery()
                ->filterByPrimaryKeys($versichertesObjekt->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVersichertesObjekt() only accepts arguments of type VersichertesObjekt or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VersichertesObjekt relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function joinVersichertesObjekt($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VersichertesObjekt');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VersichertesObjekt');
        }

        return $this;
    }

    /**
     * Use the VersichertesObjekt relation VersichertesObjekt object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   VersichertesObjektQuery A secondary query class using the current class as primary query
     */
    public function useVersichertesObjektQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVersichertesObjekt($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VersichertesObjekt', 'VersichertesObjektQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Stamm $stamm Object to remove from the list of results
     *
     * @return StammQuery The current query, for fluid interface
     */
    public function prune($stamm = null)
    {
        if ($stamm) {
            $this->addUsingAlias(StammPeer::ID, $stamm->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
