<?php


/**
 * Base class that represents a query for the 'versichertes_objekt' table.
 *
 *
 *
 * @method VersichertesObjektQuery orderById($order = Criteria::ASC) Order by the id column
 * @method VersichertesObjektQuery orderByMod($order = Criteria::ASC) Order by the mod column
 * @method VersichertesObjektQuery orderByVersicherungsnummer($order = Criteria::ASC) Order by the versicherungsnummer column
 * @method VersichertesObjektQuery orderByBeschreibung($order = Criteria::ASC) Order by the beschreibung column
 * @method VersichertesObjektQuery orderByKommentar($order = Criteria::ASC) Order by the kommentar column
 * @method VersichertesObjektQuery orderByStammId($order = Criteria::ASC) Order by the stamm_id column
 * @method VersichertesObjektQuery orderByBasispramie($order = Criteria::ASC) Order by the basispramie column
 * @method VersichertesObjektQuery orderByZahlweise($order = Criteria::ASC) Order by the zahlweise column
 * @method VersichertesObjektQuery orderByIban($order = Criteria::ASC) Order by the iban column
 * @method VersichertesObjektQuery orderByBic($order = Criteria::ASC) Order by the bic column
 *
 * @method VersichertesObjektQuery groupById() Group by the id column
 * @method VersichertesObjektQuery groupByMod() Group by the mod column
 * @method VersichertesObjektQuery groupByVersicherungsnummer() Group by the versicherungsnummer column
 * @method VersichertesObjektQuery groupByBeschreibung() Group by the beschreibung column
 * @method VersichertesObjektQuery groupByKommentar() Group by the kommentar column
 * @method VersichertesObjektQuery groupByStammId() Group by the stamm_id column
 * @method VersichertesObjektQuery groupByBasispramie() Group by the basispramie column
 * @method VersichertesObjektQuery groupByZahlweise() Group by the zahlweise column
 * @method VersichertesObjektQuery groupByIban() Group by the iban column
 * @method VersichertesObjektQuery groupByBic() Group by the bic column
 *
 * @method VersichertesObjektQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method VersichertesObjektQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method VersichertesObjektQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method VersichertesObjektQuery leftJoinStamm($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stamm relation
 * @method VersichertesObjektQuery rightJoinStamm($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stamm relation
 * @method VersichertesObjektQuery innerJoinStamm($relationAlias = null) Adds a INNER JOIN clause to the query using the Stamm relation
 *
 * @method VersichertesObjektQuery leftJoinBeitrag($relationAlias = null) Adds a LEFT JOIN clause to the query using the Beitrag relation
 * @method VersichertesObjektQuery rightJoinBeitrag($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Beitrag relation
 * @method VersichertesObjektQuery innerJoinBeitrag($relationAlias = null) Adds a INNER JOIN clause to the query using the Beitrag relation
 *
 * @method VersichertesObjekt findOne(PropelPDO $con = null) Return the first VersichertesObjekt matching the query
 * @method VersichertesObjekt findOneOrCreate(PropelPDO $con = null) Return the first VersichertesObjekt matching the query, or a new VersichertesObjekt object populated from the query conditions when no match is found
 *
 * @method VersichertesObjekt findOneByMod(int $mod) Return the first VersichertesObjekt filtered by the mod column
 * @method VersichertesObjekt findOneByVersicherungsnummer(int $versicherungsnummer) Return the first VersichertesObjekt filtered by the versicherungsnummer column
 * @method VersichertesObjekt findOneByBeschreibung(string $beschreibung) Return the first VersichertesObjekt filtered by the beschreibung column
 * @method VersichertesObjekt findOneByKommentar(string $kommentar) Return the first VersichertesObjekt filtered by the kommentar column
 * @method VersichertesObjekt findOneByStammId(int $stamm_id) Return the first VersichertesObjekt filtered by the stamm_id column
 * @method VersichertesObjekt findOneByBasispramie(double $basispramie) Return the first VersichertesObjekt filtered by the basispramie column
 * @method VersichertesObjekt findOneByZahlweise(int $zahlweise) Return the first VersichertesObjekt filtered by the zahlweise column
 * @method VersichertesObjekt findOneByIban(string $iban) Return the first VersichertesObjekt filtered by the iban column
 * @method VersichertesObjekt findOneByBic(string $bic) Return the first VersichertesObjekt filtered by the bic column
 *
 * @method array findById(int $id) Return VersichertesObjekt objects filtered by the id column
 * @method array findByMod(int $mod) Return VersichertesObjekt objects filtered by the mod column
 * @method array findByVersicherungsnummer(int $versicherungsnummer) Return VersichertesObjekt objects filtered by the versicherungsnummer column
 * @method array findByBeschreibung(string $beschreibung) Return VersichertesObjekt objects filtered by the beschreibung column
 * @method array findByKommentar(string $kommentar) Return VersichertesObjekt objects filtered by the kommentar column
 * @method array findByStammId(int $stamm_id) Return VersichertesObjekt objects filtered by the stamm_id column
 * @method array findByBasispramie(double $basispramie) Return VersichertesObjekt objects filtered by the basispramie column
 * @method array findByZahlweise(int $zahlweise) Return VersichertesObjekt objects filtered by the zahlweise column
 * @method array findByIban(string $iban) Return VersichertesObjekt objects filtered by the iban column
 * @method array findByBic(string $bic) Return VersichertesObjekt objects filtered by the bic column
 *
 * @package    propel.generator.propel.om
 */
abstract class BaseVersichertesObjektQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseVersichertesObjektQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'mversicherung';
        }
        if (null === $modelName) {
            $modelName = 'VersichertesObjekt';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new VersichertesObjektQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   VersichertesObjektQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return VersichertesObjektQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof VersichertesObjektQuery) {
            return $criteria;
        }
        $query = new VersichertesObjektQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   VersichertesObjekt|VersichertesObjekt[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = VersichertesObjektPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(VersichertesObjektPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 VersichertesObjekt A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 VersichertesObjekt A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `mod`, `versicherungsnummer`, `beschreibung`, `kommentar`, `stamm_id`, `basispramie`, `zahlweise`, `iban`, `bic` FROM `versichertes_objekt` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new VersichertesObjekt();
            $obj->hydrate($row);
            VersichertesObjektPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return VersichertesObjekt|VersichertesObjekt[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|VersichertesObjekt[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(VersichertesObjektPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(VersichertesObjektPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(VersichertesObjektPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(VersichertesObjektPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersichertesObjektPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the mod column
     *
     * Example usage:
     * <code>
     * $query->filterByMod(1234); // WHERE mod = 1234
     * $query->filterByMod(array(12, 34)); // WHERE mod IN (12, 34)
     * $query->filterByMod(array('min' => 12)); // WHERE mod >= 12
     * $query->filterByMod(array('max' => 12)); // WHERE mod <= 12
     * </code>
     *
     * @param     mixed $mod The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterByMod($mod = null, $comparison = null)
    {
        if (is_array($mod)) {
            $useMinMax = false;
            if (isset($mod['min'])) {
                $this->addUsingAlias(VersichertesObjektPeer::MOD, $mod['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mod['max'])) {
                $this->addUsingAlias(VersichertesObjektPeer::MOD, $mod['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersichertesObjektPeer::MOD, $mod, $comparison);
    }

    /**
     * Filter the query on the versicherungsnummer column
     *
     * Example usage:
     * <code>
     * $query->filterByVersicherungsnummer(1234); // WHERE versicherungsnummer = 1234
     * $query->filterByVersicherungsnummer(array(12, 34)); // WHERE versicherungsnummer IN (12, 34)
     * $query->filterByVersicherungsnummer(array('min' => 12)); // WHERE versicherungsnummer >= 12
     * $query->filterByVersicherungsnummer(array('max' => 12)); // WHERE versicherungsnummer <= 12
     * </code>
     *
     * @param     mixed $versicherungsnummer The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterByVersicherungsnummer($versicherungsnummer = null, $comparison = null)
    {
        if (is_array($versicherungsnummer)) {
            $useMinMax = false;
            if (isset($versicherungsnummer['min'])) {
                $this->addUsingAlias(VersichertesObjektPeer::VERSICHERUNGSNUMMER, $versicherungsnummer['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($versicherungsnummer['max'])) {
                $this->addUsingAlias(VersichertesObjektPeer::VERSICHERUNGSNUMMER, $versicherungsnummer['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersichertesObjektPeer::VERSICHERUNGSNUMMER, $versicherungsnummer, $comparison);
    }

    /**
     * Filter the query on the beschreibung column
     *
     * Example usage:
     * <code>
     * $query->filterByBeschreibung('fooValue');   // WHERE beschreibung = 'fooValue'
     * $query->filterByBeschreibung('%fooValue%'); // WHERE beschreibung LIKE '%fooValue%'
     * </code>
     *
     * @param     string $beschreibung The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterByBeschreibung($beschreibung = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($beschreibung)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $beschreibung)) {
                $beschreibung = str_replace('*', '%', $beschreibung);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VersichertesObjektPeer::BESCHREIBUNG, $beschreibung, $comparison);
    }

    /**
     * Filter the query on the kommentar column
     *
     * Example usage:
     * <code>
     * $query->filterByKommentar('fooValue');   // WHERE kommentar = 'fooValue'
     * $query->filterByKommentar('%fooValue%'); // WHERE kommentar LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kommentar The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterByKommentar($kommentar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kommentar)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kommentar)) {
                $kommentar = str_replace('*', '%', $kommentar);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VersichertesObjektPeer::KOMMENTAR, $kommentar, $comparison);
    }

    /**
     * Filter the query on the stamm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStammId(1234); // WHERE stamm_id = 1234
     * $query->filterByStammId(array(12, 34)); // WHERE stamm_id IN (12, 34)
     * $query->filterByStammId(array('min' => 12)); // WHERE stamm_id >= 12
     * $query->filterByStammId(array('max' => 12)); // WHERE stamm_id <= 12
     * </code>
     *
     * @see       filterByStamm()
     *
     * @param     mixed $stammId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterByStammId($stammId = null, $comparison = null)
    {
        if (is_array($stammId)) {
            $useMinMax = false;
            if (isset($stammId['min'])) {
                $this->addUsingAlias(VersichertesObjektPeer::STAMM_ID, $stammId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($stammId['max'])) {
                $this->addUsingAlias(VersichertesObjektPeer::STAMM_ID, $stammId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersichertesObjektPeer::STAMM_ID, $stammId, $comparison);
    }

    /**
     * Filter the query on the basispramie column
     *
     * Example usage:
     * <code>
     * $query->filterByBasispramie(1234); // WHERE basispramie = 1234
     * $query->filterByBasispramie(array(12, 34)); // WHERE basispramie IN (12, 34)
     * $query->filterByBasispramie(array('min' => 12)); // WHERE basispramie >= 12
     * $query->filterByBasispramie(array('max' => 12)); // WHERE basispramie <= 12
     * </code>
     *
     * @param     mixed $basispramie The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterByBasispramie($basispramie = null, $comparison = null)
    {
        if (is_array($basispramie)) {
            $useMinMax = false;
            if (isset($basispramie['min'])) {
                $this->addUsingAlias(VersichertesObjektPeer::BASISPRAMIE, $basispramie['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($basispramie['max'])) {
                $this->addUsingAlias(VersichertesObjektPeer::BASISPRAMIE, $basispramie['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersichertesObjektPeer::BASISPRAMIE, $basispramie, $comparison);
    }

    /**
     * Filter the query on the zahlweise column
     *
     * Example usage:
     * <code>
     * $query->filterByZahlweise(1234); // WHERE zahlweise = 1234
     * $query->filterByZahlweise(array(12, 34)); // WHERE zahlweise IN (12, 34)
     * $query->filterByZahlweise(array('min' => 12)); // WHERE zahlweise >= 12
     * $query->filterByZahlweise(array('max' => 12)); // WHERE zahlweise <= 12
     * </code>
     *
     * @param     mixed $zahlweise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterByZahlweise($zahlweise = null, $comparison = null)
    {
        if (is_array($zahlweise)) {
            $useMinMax = false;
            if (isset($zahlweise['min'])) {
                $this->addUsingAlias(VersichertesObjektPeer::ZAHLWEISE, $zahlweise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($zahlweise['max'])) {
                $this->addUsingAlias(VersichertesObjektPeer::ZAHLWEISE, $zahlweise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersichertesObjektPeer::ZAHLWEISE, $zahlweise, $comparison);
    }

    /**
     * Filter the query on the iban column
     *
     * Example usage:
     * <code>
     * $query->filterByIban('fooValue');   // WHERE iban = 'fooValue'
     * $query->filterByIban('%fooValue%'); // WHERE iban LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iban The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterByIban($iban = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iban)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $iban)) {
                $iban = str_replace('*', '%', $iban);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VersichertesObjektPeer::IBAN, $iban, $comparison);
    }

    /**
     * Filter the query on the bic column
     *
     * Example usage:
     * <code>
     * $query->filterByBic('fooValue');   // WHERE bic = 'fooValue'
     * $query->filterByBic('%fooValue%'); // WHERE bic LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bic The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function filterByBic($bic = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bic)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bic)) {
                $bic = str_replace('*', '%', $bic);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VersichertesObjektPeer::BIC, $bic, $comparison);
    }

    /**
     * Filter the query by a related Stamm object
     *
     * @param   Stamm|PropelObjectCollection $stamm The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 VersichertesObjektQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByStamm($stamm, $comparison = null)
    {
        if ($stamm instanceof Stamm) {
            return $this
                ->addUsingAlias(VersichertesObjektPeer::STAMM_ID, $stamm->getId(), $comparison);
        } elseif ($stamm instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(VersichertesObjektPeer::STAMM_ID, $stamm->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStamm() only accepts arguments of type Stamm or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stamm relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function joinStamm($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stamm');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stamm');
        }

        return $this;
    }

    /**
     * Use the Stamm relation Stamm object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   StammQuery A secondary query class using the current class as primary query
     */
    public function useStammQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStamm($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stamm', 'StammQuery');
    }

    /**
     * Filter the query by a related Beitrage object
     *
     * @param   Beitrage|PropelObjectCollection $beitrage  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 VersichertesObjektQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBeitrag($beitrage, $comparison = null)
    {
        if ($beitrage instanceof Beitrage) {
            return $this
                ->addUsingAlias(VersichertesObjektPeer::ID, $beitrage->getVoId(), $comparison);
        } elseif ($beitrage instanceof PropelObjectCollection) {
            return $this
                ->useBeitragQuery()
                ->filterByPrimaryKeys($beitrage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBeitrag() only accepts arguments of type Beitrage or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Beitrag relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function joinBeitrag($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Beitrag');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Beitrag');
        }

        return $this;
    }

    /**
     * Use the Beitrag relation Beitrage object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   BeitrageQuery A secondary query class using the current class as primary query
     */
    public function useBeitragQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBeitrag($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Beitrag', 'BeitrageQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   VersichertesObjekt $versichertesObjekt Object to remove from the list of results
     *
     * @return VersichertesObjektQuery The current query, for fluid interface
     */
    public function prune($versichertesObjekt = null)
    {
        if ($versichertesObjekt) {
            $this->addUsingAlias(VersichertesObjektPeer::ID, $versichertesObjekt->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
