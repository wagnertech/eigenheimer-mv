<?php


/**
 * Base class that represents a row from the 'stamm' table.
 *
 *
 *
 * @package    propel.generator.propel.om
 */
abstract class BaseStamm extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'StammPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        StammPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the mod field.
     * @var        int
     */
    protected $mod;

    /**
     * The value for the nachname field.
     * @var        string
     */
    protected $nachname;

    /**
     * The value for the vorname field.
     * @var        string
     */
    protected $vorname;

    /**
     * The value for the stasse field.
     * @var        string
     */
    protected $stasse;

    /**
     * The value for the plz field.
     * @var        string
     */
    protected $plz;

    /**
     * The value for the ort field.
     * @var        string
     */
    protected $ort;

    /**
     * The value for the zahlweise field.
     * @var        int
     */
    protected $zahlweise;

    /**
     * The value for the iban field.
     * @var        string
     */
    protected $iban;

    /**
     * The value for the bic field.
     * @var        string
     */
    protected $bic;

    /**
     * The value for the kommentar field.
     * @var        string
     */
    protected $kommentar;

    /**
     * The value for the kundigung field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $kundigung;

    /**
     * @var        PropelObjectCollection|VersichertesObjekt[] Collection to store aggregation of VersichertesObjekt objects.
     */
    protected $collVersichertesObjekts;
    protected $collVersichertesObjektsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $versichertesObjektsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->kundigung = 0;
    }

    /**
     * Initializes internal state of BaseStamm object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [mod] column value.
     *
     * @return int
     */
    public function getMod()
    {

        return $this->mod;
    }

    /**
     * Get the [nachname] column value.
     *
     * @return string
     */
    public function getNachname()
    {

        return $this->nachname;
    }

    /**
     * Get the [vorname] column value.
     *
     * @return string
     */
    public function getVorname()
    {

        return $this->vorname;
    }

    /**
     * Get the [stasse] column value.
     *
     * @return string
     */
    public function getStasse()
    {

        return $this->stasse;
    }

    /**
     * Get the [plz] column value.
     *
     * @return string
     */
    public function getPlz()
    {

        return $this->plz;
    }

    /**
     * Get the [ort] column value.
     *
     * @return string
     */
    public function getOrt()
    {

        return $this->ort;
    }

    /**
     * Get the [zahlweise] column value.
     *
     * @return int
     */
    public function getZahlweise()
    {

        return $this->zahlweise;
    }

    /**
     * Get the [iban] column value.
     *
     * @return string
     */
    public function getIban()
    {

        return $this->iban;
    }

    /**
     * Get the [bic] column value.
     *
     * @return string
     */
    public function getBic()
    {

        return $this->bic;
    }

    /**
     * Get the [kommentar] column value.
     *
     * @return string
     */
    public function getKommentar()
    {

        return $this->kommentar;
    }

    /**
     * Get the [kundigung] column value.
     *
     * @return int
     */
    public function getKundigung()
    {

        return $this->kundigung;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = StammPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [mod] column.
     *
     * @param  int $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setMod($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->mod !== $v) {
            $this->mod = $v;
            $this->modifiedColumns[] = StammPeer::MOD;
        }


        return $this;
    } // setMod()

    /**
     * Set the value of [nachname] column.
     *
     * @param  string $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setNachname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nachname !== $v) {
            $this->nachname = $v;
            $this->modifiedColumns[] = StammPeer::NACHNAME;
        }


        return $this;
    } // setNachname()

    /**
     * Set the value of [vorname] column.
     *
     * @param  string $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setVorname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vorname !== $v) {
            $this->vorname = $v;
            $this->modifiedColumns[] = StammPeer::VORNAME;
        }


        return $this;
    } // setVorname()

    /**
     * Set the value of [stasse] column.
     *
     * @param  string $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setStasse($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->stasse !== $v) {
            $this->stasse = $v;
            $this->modifiedColumns[] = StammPeer::STASSE;
        }


        return $this;
    } // setStasse()

    /**
     * Set the value of [plz] column.
     *
     * @param  string $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setPlz($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->plz !== $v) {
            $this->plz = $v;
            $this->modifiedColumns[] = StammPeer::PLZ;
        }


        return $this;
    } // setPlz()

    /**
     * Set the value of [ort] column.
     *
     * @param  string $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setOrt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ort !== $v) {
            $this->ort = $v;
            $this->modifiedColumns[] = StammPeer::ORT;
        }


        return $this;
    } // setOrt()

    /**
     * Set the value of [zahlweise] column.
     *
     * @param  int $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setZahlweise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->zahlweise !== $v) {
            $this->zahlweise = $v;
            $this->modifiedColumns[] = StammPeer::ZAHLWEISE;
        }


        return $this;
    } // setZahlweise()

    /**
     * Set the value of [iban] column.
     *
     * @param  string $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setIban($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->iban !== $v) {
            $this->iban = $v;
            $this->modifiedColumns[] = StammPeer::IBAN;
        }


        return $this;
    } // setIban()

    /**
     * Set the value of [bic] column.
     *
     * @param  string $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setBic($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bic !== $v) {
            $this->bic = $v;
            $this->modifiedColumns[] = StammPeer::BIC;
        }


        return $this;
    } // setBic()

    /**
     * Set the value of [kommentar] column.
     *
     * @param  string $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setKommentar($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->kommentar !== $v) {
            $this->kommentar = $v;
            $this->modifiedColumns[] = StammPeer::KOMMENTAR;
        }


        return $this;
    } // setKommentar()

    /**
     * Set the value of [kundigung] column.
     *
     * @param  int $v new value
     * @return Stamm The current object (for fluent API support)
     */
    public function setKundigung($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kundigung !== $v) {
            $this->kundigung = $v;
            $this->modifiedColumns[] = StammPeer::KUNDIGUNG;
        }


        return $this;
    } // setKundigung()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->kundigung !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->mod = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->nachname = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->vorname = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->stasse = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->plz = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->ort = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->zahlweise = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->iban = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->bic = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->kommentar = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->kundigung = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 12; // 12 = StammPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Stamm object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(StammPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = StammPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collVersichertesObjekts = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(StammPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = StammQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(StammPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                StammPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->versichertesObjektsScheduledForDeletion !== null) {
                if (!$this->versichertesObjektsScheduledForDeletion->isEmpty()) {
                    VersichertesObjektQuery::create()
                        ->filterByPrimaryKeys($this->versichertesObjektsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->versichertesObjektsScheduledForDeletion = null;
                }
            }

            if ($this->collVersichertesObjekts !== null) {
                foreach ($this->collVersichertesObjekts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = StammPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . StammPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(StammPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(StammPeer::MOD)) {
            $modifiedColumns[':p' . $index++]  = '`mod`';
        }
        if ($this->isColumnModified(StammPeer::NACHNAME)) {
            $modifiedColumns[':p' . $index++]  = '`nachname`';
        }
        if ($this->isColumnModified(StammPeer::VORNAME)) {
            $modifiedColumns[':p' . $index++]  = '`vorname`';
        }
        if ($this->isColumnModified(StammPeer::STASSE)) {
            $modifiedColumns[':p' . $index++]  = '`stasse`';
        }
        if ($this->isColumnModified(StammPeer::PLZ)) {
            $modifiedColumns[':p' . $index++]  = '`plz`';
        }
        if ($this->isColumnModified(StammPeer::ORT)) {
            $modifiedColumns[':p' . $index++]  = '`ort`';
        }
        if ($this->isColumnModified(StammPeer::ZAHLWEISE)) {
            $modifiedColumns[':p' . $index++]  = '`zahlweise`';
        }
        if ($this->isColumnModified(StammPeer::IBAN)) {
            $modifiedColumns[':p' . $index++]  = '`iban`';
        }
        if ($this->isColumnModified(StammPeer::BIC)) {
            $modifiedColumns[':p' . $index++]  = '`bic`';
        }
        if ($this->isColumnModified(StammPeer::KOMMENTAR)) {
            $modifiedColumns[':p' . $index++]  = '`kommentar`';
        }
        if ($this->isColumnModified(StammPeer::KUNDIGUNG)) {
            $modifiedColumns[':p' . $index++]  = '`kundigung`';
        }

        $sql = sprintf(
            'INSERT INTO `stamm` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`mod`':
                        $stmt->bindValue($identifier, $this->mod, PDO::PARAM_INT);
                        break;
                    case '`nachname`':
                        $stmt->bindValue($identifier, $this->nachname, PDO::PARAM_STR);
                        break;
                    case '`vorname`':
                        $stmt->bindValue($identifier, $this->vorname, PDO::PARAM_STR);
                        break;
                    case '`stasse`':
                        $stmt->bindValue($identifier, $this->stasse, PDO::PARAM_STR);
                        break;
                    case '`plz`':
                        $stmt->bindValue($identifier, $this->plz, PDO::PARAM_STR);
                        break;
                    case '`ort`':
                        $stmt->bindValue($identifier, $this->ort, PDO::PARAM_STR);
                        break;
                    case '`zahlweise`':
                        $stmt->bindValue($identifier, $this->zahlweise, PDO::PARAM_INT);
                        break;
                    case '`iban`':
                        $stmt->bindValue($identifier, $this->iban, PDO::PARAM_STR);
                        break;
                    case '`bic`':
                        $stmt->bindValue($identifier, $this->bic, PDO::PARAM_STR);
                        break;
                    case '`kommentar`':
                        $stmt->bindValue($identifier, $this->kommentar, PDO::PARAM_STR);
                        break;
                    case '`kundigung`':
                        $stmt->bindValue($identifier, $this->kundigung, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = StammPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collVersichertesObjekts !== null) {
                    foreach ($this->collVersichertesObjekts as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = StammPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getMod();
                break;
            case 2:
                return $this->getNachname();
                break;
            case 3:
                return $this->getVorname();
                break;
            case 4:
                return $this->getStasse();
                break;
            case 5:
                return $this->getPlz();
                break;
            case 6:
                return $this->getOrt();
                break;
            case 7:
                return $this->getZahlweise();
                break;
            case 8:
                return $this->getIban();
                break;
            case 9:
                return $this->getBic();
                break;
            case 10:
                return $this->getKommentar();
                break;
            case 11:
                return $this->getKundigung();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Stamm'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Stamm'][$this->getPrimaryKey()] = true;
        $keys = StammPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getMod(),
            $keys[2] => $this->getNachname(),
            $keys[3] => $this->getVorname(),
            $keys[4] => $this->getStasse(),
            $keys[5] => $this->getPlz(),
            $keys[6] => $this->getOrt(),
            $keys[7] => $this->getZahlweise(),
            $keys[8] => $this->getIban(),
            $keys[9] => $this->getBic(),
            $keys[10] => $this->getKommentar(),
            $keys[11] => $this->getKundigung(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collVersichertesObjekts) {
                $result['VersichertesObjekts'] = $this->collVersichertesObjekts->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = StammPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setMod($value);
                break;
            case 2:
                $this->setNachname($value);
                break;
            case 3:
                $this->setVorname($value);
                break;
            case 4:
                $this->setStasse($value);
                break;
            case 5:
                $this->setPlz($value);
                break;
            case 6:
                $this->setOrt($value);
                break;
            case 7:
                $this->setZahlweise($value);
                break;
            case 8:
                $this->setIban($value);
                break;
            case 9:
                $this->setBic($value);
                break;
            case 10:
                $this->setKommentar($value);
                break;
            case 11:
                $this->setKundigung($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = StammPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setMod($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNachname($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setVorname($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setStasse($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPlz($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setOrt($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setZahlweise($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setIban($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setBic($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setKommentar($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setKundigung($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(StammPeer::DATABASE_NAME);

        if ($this->isColumnModified(StammPeer::ID)) $criteria->add(StammPeer::ID, $this->id);
        if ($this->isColumnModified(StammPeer::MOD)) $criteria->add(StammPeer::MOD, $this->mod);
        if ($this->isColumnModified(StammPeer::NACHNAME)) $criteria->add(StammPeer::NACHNAME, $this->nachname);
        if ($this->isColumnModified(StammPeer::VORNAME)) $criteria->add(StammPeer::VORNAME, $this->vorname);
        if ($this->isColumnModified(StammPeer::STASSE)) $criteria->add(StammPeer::STASSE, $this->stasse);
        if ($this->isColumnModified(StammPeer::PLZ)) $criteria->add(StammPeer::PLZ, $this->plz);
        if ($this->isColumnModified(StammPeer::ORT)) $criteria->add(StammPeer::ORT, $this->ort);
        if ($this->isColumnModified(StammPeer::ZAHLWEISE)) $criteria->add(StammPeer::ZAHLWEISE, $this->zahlweise);
        if ($this->isColumnModified(StammPeer::IBAN)) $criteria->add(StammPeer::IBAN, $this->iban);
        if ($this->isColumnModified(StammPeer::BIC)) $criteria->add(StammPeer::BIC, $this->bic);
        if ($this->isColumnModified(StammPeer::KOMMENTAR)) $criteria->add(StammPeer::KOMMENTAR, $this->kommentar);
        if ($this->isColumnModified(StammPeer::KUNDIGUNG)) $criteria->add(StammPeer::KUNDIGUNG, $this->kundigung);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(StammPeer::DATABASE_NAME);
        $criteria->add(StammPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Stamm (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setMod($this->getMod());
        $copyObj->setNachname($this->getNachname());
        $copyObj->setVorname($this->getVorname());
        $copyObj->setStasse($this->getStasse());
        $copyObj->setPlz($this->getPlz());
        $copyObj->setOrt($this->getOrt());
        $copyObj->setZahlweise($this->getZahlweise());
        $copyObj->setIban($this->getIban());
        $copyObj->setBic($this->getBic());
        $copyObj->setKommentar($this->getKommentar());
        $copyObj->setKundigung($this->getKundigung());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getVersichertesObjekts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVersichertesObjekt($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Stamm Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return StammPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new StammPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('VersichertesObjekt' == $relationName) {
            $this->initVersichertesObjekts();
        }
    }

    /**
     * Clears out the collVersichertesObjekts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Stamm The current object (for fluent API support)
     * @see        addVersichertesObjekts()
     */
    public function clearVersichertesObjekts()
    {
        $this->collVersichertesObjekts = null; // important to set this to null since that means it is uninitialized
        $this->collVersichertesObjektsPartial = null;

        return $this;
    }

    /**
     * reset is the collVersichertesObjekts collection loaded partially
     *
     * @return void
     */
    public function resetPartialVersichertesObjekts($v = true)
    {
        $this->collVersichertesObjektsPartial = $v;
    }

    /**
     * Initializes the collVersichertesObjekts collection.
     *
     * By default this just sets the collVersichertesObjekts collection to an empty array (like clearcollVersichertesObjekts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVersichertesObjekts($overrideExisting = true)
    {
        if (null !== $this->collVersichertesObjekts && !$overrideExisting) {
            return;
        }
        $this->collVersichertesObjekts = new PropelObjectCollection();
        $this->collVersichertesObjekts->setModel('VersichertesObjekt');
    }

    /**
     * Gets an array of VersichertesObjekt objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Stamm is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VersichertesObjekt[] List of VersichertesObjekt objects
     * @throws PropelException
     */
    public function getVersichertesObjekts($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVersichertesObjektsPartial && !$this->isNew();
        if (null === $this->collVersichertesObjekts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVersichertesObjekts) {
                // return empty collection
                $this->initVersichertesObjekts();
            } else {
                $collVersichertesObjekts = VersichertesObjektQuery::create(null, $criteria)
                    ->filterByStamm($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVersichertesObjektsPartial && count($collVersichertesObjekts)) {
                      $this->initVersichertesObjekts(false);

                      foreach ($collVersichertesObjekts as $obj) {
                        if (false == $this->collVersichertesObjekts->contains($obj)) {
                          $this->collVersichertesObjekts->append($obj);
                        }
                      }

                      $this->collVersichertesObjektsPartial = true;
                    }

                    $collVersichertesObjekts->getInternalIterator()->rewind();

                    return $collVersichertesObjekts;
                }

                if ($partial && $this->collVersichertesObjekts) {
                    foreach ($this->collVersichertesObjekts as $obj) {
                        if ($obj->isNew()) {
                            $collVersichertesObjekts[] = $obj;
                        }
                    }
                }

                $this->collVersichertesObjekts = $collVersichertesObjekts;
                $this->collVersichertesObjektsPartial = false;
            }
        }

        return $this->collVersichertesObjekts;
    }

    /**
     * Sets a collection of VersichertesObjekt objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $versichertesObjekts A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Stamm The current object (for fluent API support)
     */
    public function setVersichertesObjekts(PropelCollection $versichertesObjekts, PropelPDO $con = null)
    {
        $versichertesObjektsToDelete = $this->getVersichertesObjekts(new Criteria(), $con)->diff($versichertesObjekts);


        $this->versichertesObjektsScheduledForDeletion = $versichertesObjektsToDelete;

        foreach ($versichertesObjektsToDelete as $versichertesObjektRemoved) {
            $versichertesObjektRemoved->setStamm(null);
        }

        $this->collVersichertesObjekts = null;
        foreach ($versichertesObjekts as $versichertesObjekt) {
            $this->addVersichertesObjekt($versichertesObjekt);
        }

        $this->collVersichertesObjekts = $versichertesObjekts;
        $this->collVersichertesObjektsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VersichertesObjekt objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VersichertesObjekt objects.
     * @throws PropelException
     */
    public function countVersichertesObjekts(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVersichertesObjektsPartial && !$this->isNew();
        if (null === $this->collVersichertesObjekts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVersichertesObjekts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getVersichertesObjekts());
            }
            $query = VersichertesObjektQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByStamm($this)
                ->count($con);
        }

        return count($this->collVersichertesObjekts);
    }

    /**
     * Method called to associate a VersichertesObjekt object to this object
     * through the VersichertesObjekt foreign key attribute.
     *
     * @param    VersichertesObjekt $l VersichertesObjekt
     * @return Stamm The current object (for fluent API support)
     */
    public function addVersichertesObjekt(VersichertesObjekt $l)
    {
        if ($this->collVersichertesObjekts === null) {
            $this->initVersichertesObjekts();
            $this->collVersichertesObjektsPartial = true;
        }

        if (!in_array($l, $this->collVersichertesObjekts->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVersichertesObjekt($l);

            if ($this->versichertesObjektsScheduledForDeletion and $this->versichertesObjektsScheduledForDeletion->contains($l)) {
                $this->versichertesObjektsScheduledForDeletion->remove($this->versichertesObjektsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	VersichertesObjekt $versichertesObjekt The versichertesObjekt object to add.
     */
    protected function doAddVersichertesObjekt($versichertesObjekt)
    {
        $this->collVersichertesObjekts[]= $versichertesObjekt;
        $versichertesObjekt->setStamm($this);
    }

    /**
     * @param	VersichertesObjekt $versichertesObjekt The versichertesObjekt object to remove.
     * @return Stamm The current object (for fluent API support)
     */
    public function removeVersichertesObjekt($versichertesObjekt)
    {
        if ($this->getVersichertesObjekts()->contains($versichertesObjekt)) {
            $this->collVersichertesObjekts->remove($this->collVersichertesObjekts->search($versichertesObjekt));
            if (null === $this->versichertesObjektsScheduledForDeletion) {
                $this->versichertesObjektsScheduledForDeletion = clone $this->collVersichertesObjekts;
                $this->versichertesObjektsScheduledForDeletion->clear();
            }
            $this->versichertesObjektsScheduledForDeletion[]= clone $versichertesObjekt;
            $versichertesObjekt->setStamm(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->mod = null;
        $this->nachname = null;
        $this->vorname = null;
        $this->stasse = null;
        $this->plz = null;
        $this->ort = null;
        $this->zahlweise = null;
        $this->iban = null;
        $this->bic = null;
        $this->kommentar = null;
        $this->kundigung = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collVersichertesObjekts) {
                foreach ($this->collVersichertesObjekts as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collVersichertesObjekts instanceof PropelCollection) {
            $this->collVersichertesObjekts->clearIterator();
        }
        $this->collVersichertesObjekts = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(StammPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
