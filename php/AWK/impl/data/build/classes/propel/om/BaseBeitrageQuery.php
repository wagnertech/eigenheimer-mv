<?php


/**
 * Base class that represents a query for the 'beitrage' table.
 *
 *
 *
 * @method BeitrageQuery orderById($order = Criteria::ASC) Order by the id column
 * @method BeitrageQuery orderByMod($order = Criteria::ASC) Order by the mod column
 * @method BeitrageQuery orderByJahr($order = Criteria::ASC) Order by the jahr column
 * @method BeitrageQuery orderByZahlweise($order = Criteria::ASC) Order by the zahlweise column
 * @method BeitrageQuery orderByBetrag($order = Criteria::ASC) Order by the betrag column
 * @method BeitrageQuery orderByKommentar($order = Criteria::ASC) Order by the kommentar column
 * @method BeitrageQuery orderByVoId($order = Criteria::ASC) Order by the vo_id column
 *
 * @method BeitrageQuery groupById() Group by the id column
 * @method BeitrageQuery groupByMod() Group by the mod column
 * @method BeitrageQuery groupByJahr() Group by the jahr column
 * @method BeitrageQuery groupByZahlweise() Group by the zahlweise column
 * @method BeitrageQuery groupByBetrag() Group by the betrag column
 * @method BeitrageQuery groupByKommentar() Group by the kommentar column
 * @method BeitrageQuery groupByVoId() Group by the vo_id column
 *
 * @method BeitrageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method BeitrageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method BeitrageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method BeitrageQuery leftJoinVersichertesObjekt($relationAlias = null) Adds a LEFT JOIN clause to the query using the VersichertesObjekt relation
 * @method BeitrageQuery rightJoinVersichertesObjekt($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VersichertesObjekt relation
 * @method BeitrageQuery innerJoinVersichertesObjekt($relationAlias = null) Adds a INNER JOIN clause to the query using the VersichertesObjekt relation
 *
 * @method Beitrage findOne(PropelPDO $con = null) Return the first Beitrage matching the query
 * @method Beitrage findOneOrCreate(PropelPDO $con = null) Return the first Beitrage matching the query, or a new Beitrage object populated from the query conditions when no match is found
 *
 * @method Beitrage findOneByMod(int $mod) Return the first Beitrage filtered by the mod column
 * @method Beitrage findOneByJahr(int $jahr) Return the first Beitrage filtered by the jahr column
 * @method Beitrage findOneByZahlweise(int $zahlweise) Return the first Beitrage filtered by the zahlweise column
 * @method Beitrage findOneByBetrag(double $betrag) Return the first Beitrage filtered by the betrag column
 * @method Beitrage findOneByKommentar(string $kommentar) Return the first Beitrage filtered by the kommentar column
 * @method Beitrage findOneByVoId(int $vo_id) Return the first Beitrage filtered by the vo_id column
 *
 * @method array findById(int $id) Return Beitrage objects filtered by the id column
 * @method array findByMod(int $mod) Return Beitrage objects filtered by the mod column
 * @method array findByJahr(int $jahr) Return Beitrage objects filtered by the jahr column
 * @method array findByZahlweise(int $zahlweise) Return Beitrage objects filtered by the zahlweise column
 * @method array findByBetrag(double $betrag) Return Beitrage objects filtered by the betrag column
 * @method array findByKommentar(string $kommentar) Return Beitrage objects filtered by the kommentar column
 * @method array findByVoId(int $vo_id) Return Beitrage objects filtered by the vo_id column
 *
 * @package    propel.generator.propel.om
 */
abstract class BaseBeitrageQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseBeitrageQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'mversicherung';
        }
        if (null === $modelName) {
            $modelName = 'Beitrage';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new BeitrageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   BeitrageQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return BeitrageQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof BeitrageQuery) {
            return $criteria;
        }
        $query = new BeitrageQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Beitrage|Beitrage[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = BeitragePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(BeitragePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Beitrage A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Beitrage A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `mod`, `jahr`, `zahlweise`, `betrag`, `kommentar`, `vo_id` FROM `beitrage` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Beitrage();
            $obj->hydrate($row);
            BeitragePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Beitrage|Beitrage[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Beitrage[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return BeitrageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BeitragePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return BeitrageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BeitragePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeitrageQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BeitragePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BeitragePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeitragePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the mod column
     *
     * Example usage:
     * <code>
     * $query->filterByMod(1234); // WHERE mod = 1234
     * $query->filterByMod(array(12, 34)); // WHERE mod IN (12, 34)
     * $query->filterByMod(array('min' => 12)); // WHERE mod >= 12
     * $query->filterByMod(array('max' => 12)); // WHERE mod <= 12
     * </code>
     *
     * @param     mixed $mod The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeitrageQuery The current query, for fluid interface
     */
    public function filterByMod($mod = null, $comparison = null)
    {
        if (is_array($mod)) {
            $useMinMax = false;
            if (isset($mod['min'])) {
                $this->addUsingAlias(BeitragePeer::MOD, $mod['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mod['max'])) {
                $this->addUsingAlias(BeitragePeer::MOD, $mod['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeitragePeer::MOD, $mod, $comparison);
    }

    /**
     * Filter the query on the jahr column
     *
     * Example usage:
     * <code>
     * $query->filterByJahr(1234); // WHERE jahr = 1234
     * $query->filterByJahr(array(12, 34)); // WHERE jahr IN (12, 34)
     * $query->filterByJahr(array('min' => 12)); // WHERE jahr >= 12
     * $query->filterByJahr(array('max' => 12)); // WHERE jahr <= 12
     * </code>
     *
     * @param     mixed $jahr The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeitrageQuery The current query, for fluid interface
     */
    public function filterByJahr($jahr = null, $comparison = null)
    {
        if (is_array($jahr)) {
            $useMinMax = false;
            if (isset($jahr['min'])) {
                $this->addUsingAlias(BeitragePeer::JAHR, $jahr['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jahr['max'])) {
                $this->addUsingAlias(BeitragePeer::JAHR, $jahr['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeitragePeer::JAHR, $jahr, $comparison);
    }

    /**
     * Filter the query on the zahlweise column
     *
     * Example usage:
     * <code>
     * $query->filterByZahlweise(1234); // WHERE zahlweise = 1234
     * $query->filterByZahlweise(array(12, 34)); // WHERE zahlweise IN (12, 34)
     * $query->filterByZahlweise(array('min' => 12)); // WHERE zahlweise >= 12
     * $query->filterByZahlweise(array('max' => 12)); // WHERE zahlweise <= 12
     * </code>
     *
     * @param     mixed $zahlweise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeitrageQuery The current query, for fluid interface
     */
    public function filterByZahlweise($zahlweise = null, $comparison = null)
    {
        if (is_array($zahlweise)) {
            $useMinMax = false;
            if (isset($zahlweise['min'])) {
                $this->addUsingAlias(BeitragePeer::ZAHLWEISE, $zahlweise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($zahlweise['max'])) {
                $this->addUsingAlias(BeitragePeer::ZAHLWEISE, $zahlweise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeitragePeer::ZAHLWEISE, $zahlweise, $comparison);
    }

    /**
     * Filter the query on the betrag column
     *
     * Example usage:
     * <code>
     * $query->filterByBetrag(1234); // WHERE betrag = 1234
     * $query->filterByBetrag(array(12, 34)); // WHERE betrag IN (12, 34)
     * $query->filterByBetrag(array('min' => 12)); // WHERE betrag >= 12
     * $query->filterByBetrag(array('max' => 12)); // WHERE betrag <= 12
     * </code>
     *
     * @param     mixed $betrag The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeitrageQuery The current query, for fluid interface
     */
    public function filterByBetrag($betrag = null, $comparison = null)
    {
        if (is_array($betrag)) {
            $useMinMax = false;
            if (isset($betrag['min'])) {
                $this->addUsingAlias(BeitragePeer::BETRAG, $betrag['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($betrag['max'])) {
                $this->addUsingAlias(BeitragePeer::BETRAG, $betrag['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeitragePeer::BETRAG, $betrag, $comparison);
    }

    /**
     * Filter the query on the kommentar column
     *
     * Example usage:
     * <code>
     * $query->filterByKommentar('fooValue');   // WHERE kommentar = 'fooValue'
     * $query->filterByKommentar('%fooValue%'); // WHERE kommentar LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kommentar The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeitrageQuery The current query, for fluid interface
     */
    public function filterByKommentar($kommentar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kommentar)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kommentar)) {
                $kommentar = str_replace('*', '%', $kommentar);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BeitragePeer::KOMMENTAR, $kommentar, $comparison);
    }

    /**
     * Filter the query on the vo_id column
     *
     * Example usage:
     * <code>
     * $query->filterByVoId(1234); // WHERE vo_id = 1234
     * $query->filterByVoId(array(12, 34)); // WHERE vo_id IN (12, 34)
     * $query->filterByVoId(array('min' => 12)); // WHERE vo_id >= 12
     * $query->filterByVoId(array('max' => 12)); // WHERE vo_id <= 12
     * </code>
     *
     * @see       filterByVersichertesObjekt()
     *
     * @param     mixed $voId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BeitrageQuery The current query, for fluid interface
     */
    public function filterByVoId($voId = null, $comparison = null)
    {
        if (is_array($voId)) {
            $useMinMax = false;
            if (isset($voId['min'])) {
                $this->addUsingAlias(BeitragePeer::VO_ID, $voId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($voId['max'])) {
                $this->addUsingAlias(BeitragePeer::VO_ID, $voId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeitragePeer::VO_ID, $voId, $comparison);
    }

    /**
     * Filter the query by a related VersichertesObjekt object
     *
     * @param   VersichertesObjekt|PropelObjectCollection $versichertesObjekt The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 BeitrageQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVersichertesObjekt($versichertesObjekt, $comparison = null)
    {
        if ($versichertesObjekt instanceof VersichertesObjekt) {
            return $this
                ->addUsingAlias(BeitragePeer::VO_ID, $versichertesObjekt->getId(), $comparison);
        } elseif ($versichertesObjekt instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BeitragePeer::VO_ID, $versichertesObjekt->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByVersichertesObjekt() only accepts arguments of type VersichertesObjekt or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VersichertesObjekt relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return BeitrageQuery The current query, for fluid interface
     */
    public function joinVersichertesObjekt($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VersichertesObjekt');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VersichertesObjekt');
        }

        return $this;
    }

    /**
     * Use the VersichertesObjekt relation VersichertesObjekt object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   VersichertesObjektQuery A secondary query class using the current class as primary query
     */
    public function useVersichertesObjektQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVersichertesObjekt($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VersichertesObjekt', 'VersichertesObjektQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Beitrage $beitrage Object to remove from the list of results
     *
     * @return BeitrageQuery The current query, for fluid interface
     */
    public function prune($beitrage = null)
    {
        if ($beitrage) {
            $this->addUsingAlias(BeitragePeer::ID, $beitrage->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
