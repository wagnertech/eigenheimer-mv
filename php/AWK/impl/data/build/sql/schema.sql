
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- stamm
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `stamm`;

CREATE TABLE `stamm`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `mod` INTEGER NOT NULL,
    `nachname` VARCHAR(100) NOT NULL,
    `vorname` VARCHAR(100) NOT NULL,
    `stasse` VARCHAR(100) NOT NULL,
    `plz` CHAR(10) NOT NULL,
    `ort` VARCHAR(100) NOT NULL,
    `zahlweise` INTEGER NOT NULL,
    `iban` CHAR(20) NOT NULL,
    `bic` CHAR(10) NOT NULL,
    `kommentar` VARCHAR(1000) NOT NULL,
    `kundigung` INTEGER DEFAULT 0 NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

-- ---------------------------------------------------------------------
-- versichertes_objekt
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `versichertes_objekt`;

CREATE TABLE `versichertes_objekt`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `mod` INTEGER NOT NULL,
    `versicherungsnummer` INTEGER NOT NULL,
    `beschreibung` VARCHAR(200) NOT NULL,
    `kommentar` VARCHAR(1000) NOT NULL,
    `stamm_id` INTEGER NOT NULL,
    `basispramie` FLOAT NOT NULL,
    `zahlweise` INTEGER NOT NULL,
    `iban` CHAR(20) NOT NULL,
    `bic` CHAR(10) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `versichertes_objekt_U_1` (`versicherungsnummer`),
    INDEX `versichertes_objekt_FI_1` (`stamm_id`),
    CONSTRAINT `versichertes_objekt_FK_1`
        FOREIGN KEY (`stamm_id`)
        REFERENCES `stamm` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB CHARACTER SET='utf8';

-- ---------------------------------------------------------------------
-- beitrage
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `beitrage`;

CREATE TABLE `beitrage`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `mod` INTEGER NOT NULL,
    `jahr` INTEGER NOT NULL,
    `zahlweise` INTEGER NOT NULL,
    `betrag` FLOAT NOT NULL,
    `kommentar` VARCHAR(1000) NOT NULL,
    `vo_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `beitrage_FI_1` (`vo_id`),
    CONSTRAINT `beitrage_FK_1`
        FOREIGN KEY (`vo_id`)
        REFERENCES `versichertes_objekt` (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
