mtestinfo

Setze PHP-Einstellung

!|test.util.CallPhp|
|set php config|etc/CallPhp.config|
|set util dir|/usr/share/php/mVersicherung/util|

mtestinfo

!|test.util.CallScript|
|call script|mtestinfo mversicherung genad|

Leere DB anlegen

!|test.util.CallScript|
|call script|mysql -uSVBaL -pSVBaL mversicherung </usr/share/php/mVersicherung/AWK/impl/data/build/sql/schema.sql|
