Anlegen Stamm

!|test.util.CallPhp|
|php interface|MVAdmin|
|php procedure|createInstance|
|set parameter|MocID|1|
|set parameter|InstanceData|<Personenstamm><Nachname>Wagner</Nachname><Vorname>Michael</Vorname><Straße>Turfstraße 18a</Straße><PLZ>81929</PLZ><Ort>München</Ort><Zahlweise>1</Zahlweise><IBAN>DE54</IBAN><BIC>GE</BIC><Kommentar>Bla</Kommentar><Versicherte_Objekte/></Personenstamm>|
|execute action|

Anlegen Versichertes Objekt

!|test.util.CallPhp|
|php interface|MVAdmin|
|php procedure|createInstance|
|set parameter|MocID|2|
|set parameter|!-InstanceData-!|<VersichertesObjekt><stamm_id>1</stamm_id><Versicherungsnummer>12345</Versicherungsnummer><Beschreibung>Turfstraße 18a, 81299 München</Beschreibung><Kommentar/><Basisprämie>39</Basisprämie><Zahlweise>1</Zahlweise><IBAN/><BIC/><Zahlungen/></VersichertesObjekt>|
|execute action|

Anlegen Zahlung

!|test.util.CallPhp|
|php interface|MVAdmin|
|php procedure|createInstance|
|set parameter|MocID|3|
|set parameter|InstanceData|<Beiträge><VO_id>1</VO_id><Jahr>2023</Jahr><Zahlweise>1</Zahlweise><Betrag>39</Betrag><Kommentar/></Beiträge>|
|execute action|
