# Mitgliederverwaltung Eigenheimerverband Bayern

Dieses Dokument dient als Kanban-Board zur Projektsteuerung.

Die Beschreibung der Software finden Sie unter https://gitlab.com/wagnertech/mversicherung

Die aktuelle Demo findet sich unter http://wagnertech.de/mVersicherung/

## Backlog - Wunschliste

Anmerkung: Angegebene Aufwandsschätzungen verstehen sich incl. MWSt.

### Installation auf einem anderen XAMP-fähigen Server
- Aufwand: 200.- pro Verein

### Moderne GUI
- Navigationsleiste
- Automatischer Update bei Änderungen
- Entitäten auf verschiegenen Reitern
- Aufwand: viel

### Dokumentenablage
- Anzeigen/Hochladen-Knopf bei jedem Stameintrag
- Aufwand: 500.-

### Excel-Export
- Auswahl Stamm/Stamm+Objekt/Stamm+Zahlung
- Export als csv-Datei
- Aufwand: 250.-

### IBAN-Prüfung
- Prüfung mit einem IBAN-Rechner, Ermittlung der Bank
- Aufwand: 250.-

## In Arbeit

### A001 - Meherere Kommentarfelder am Stamm und am Objekt
- Realisiert als nicht-editierbare Liste
- Aufwand: 75.-

### A002 - Installation auf wagnertech.de incl. aller gewünschten Felder
- Aufwand: 100.- pro Verein

### A003 - Funktion Bankeinzug
- Erstellung einer HBCI-Datei (z.B. für Münchner Bank)
- Nachverfolgbar, kann rückgängig gemacht werden
- Aufwand: 500.-

### A004 - Datenmigration aus csv/Excel
- Aufwand: 200.- pro Verein

### A005 - Export Mailadressen
- Aufwand: 75.-

### A006 - Zahlungsübersicht
- Aufwand: 100.-

### A007 - Ausweiserstellung
- Auswahl: Einzelmitglied/alle Mitglider
- Export als PDF-Datei
- Vorlage als PDF
- Aufwand: 150.- pro Verein

## Im Test

## Erledigt

